require "global"
require "messages"

platform = ""
outsideZone = true
language = "de"

Grad_11 = 0
Meter_11 = 0

function Cartridge_OnStart()
    if Env.Platform == "Vendor 1 ARM9" then
        platform = "Garmin"
    elseif string.sub(Env.Platform, 1, 6) == "iPhone" then
        platform = "IPhone"
    else
        platform = "Android"
    end
end

function Stage01_OnEnter()
    if outsideZone then
        outsideZone = false
        msgMitNachricht(getMessage(language, 81),
            function()
                I_Rucksack:MoveTo(Player)
                V_Rucksackauswahl = false
                Zone01.Active = false
                Zone02.Active = true
                if platform == "Android" or platform == "IPhone" then
                    Wherigo.PlayAudio(S_RudolphMelodie)
                end
                outsideZone = true
                Wherigo.ShowScreen(Wherigo.MAINSCREEN)
            end)
    end
end

function Stage02_OnEnter()
    if outsideZone then
        outsideZone = false
        I_DasTreffen:MoveTo(Player)
        Wherigo.GetInput(Q_DasTreffen)
    end
end

function Stage03_OnEnter()
    if outsideZone then
        outsideZone = false
        msgMitNachrichtUndBild(getMessage(language, 82), P_CacherVerarztetRudolph,
            function()
                V_Rucksackauswahl = true
                Wherigo.ShowScreen(Wherigo.MAINSCREEN)
            end)
    end
end

function Stage04_OnEnter()
    if outsideZone then
        outsideZone = false
        msgMitNachrichtUndBild(getMessage(language, 85), P_RudolphVerletztVerbunden,
            function()
                I_Rucksack:MoveTo(nil)
                I_WFragen:MoveTo(Player)
                T_Die5W.Active = true
                Wherigo.ShowScreen(Wherigo.MAINSCREEN)
            end)
    end
end

function Stage05_OnEnter()
    if outsideZone then
        outsideZone = false
        msgMitNachricht(getMessage(language, 96),
            function()
                I_Tierarztpraxis:MoveTo(Player)
                Wherigo.GetInput(Q_Tierarztpraxis)
            end)
    end
end

function Stage06_OnEnter()
    if outsideZone then
        outsideZone = false
        T_TelefonzelleZurueck.Active = false
        Z_TelephoneHelper.Active = false
        I_Telefon:MoveTo(Player)
        Wherigo.GetInput(Q_Telefon)
    end
end

function Stage07_OnEnter()
    if outsideZone then
        outsideZone = false
        msgMitNachricht(getMessage(language, 101),
            function()
                I_Weltkugel:MoveTo(Player)
                Wherigo.GetInput(Q_Weltkugel)
            end)
    elseif outsideZone and V_Strafzone then
        I_Weltkugel:MoveTo(Player)
        Wherigo.GetInput(Q_Weltkugel)
    end
end

function Stage08_OnEnter()
    if outsideZone then
        outsideZone = false
        msgMitNachricht(getMessage(language, 106),
            function()
                I_SeineFreunde:MoveTo(Player)
                Wherigo.GetInput(Q_SeineFreunde)
            end)
    end
end

function Stage09_OnEnter()
    if outsideZone then
        outsideZone = false
        msgMitNachrichtUndBild(getMessage(language, 113), P_RudolphMitHase,
            function()
                msgMitNachricht(getMessage(language, 114),
                    function()
                        T_HaseKarotteFuchs.Active = true
                        Zone09_Hase.Active = false
                        Zone09_Ufer1.Active = true
                        Zone09_Ufer2.Active = true
                        outsideZone = true
                        Wherigo.ShowScreen(Wherigo.MAINSCREEN)
                    end)
            end)
    end
end

function Stage09_Ufer1_OnExit()
    HaseKarotteFuchs()
end

function Stage09_Ufer2_OnExit()
    HaseKarotteFuchs()
end

function Stage10_OnEnter()
    if outsideZone then
        outsideZone = false
        msgMitNachricht(getMessage(language, 120),
            function()
                I_Suess:MoveTo(Player)
                Wherigo.GetInput(Q_Suess)
            end)
    end
end

function Stage11_OnEnter()
    if outsideZone then
        outsideZone = false
        msgMitNachrichtUndBild(getMessage(language, 138), P_Versteck,
            function()
                T_Inselpeilung.Active = false
                T_ZuFindendesObjekt.Active = true
                Referenz_11.Active = false
                Zone11.Active = false
                Zone12.Active = true
                V_DieInsel = false
                outsideZone = true
                Wherigo.ShowScreen(Wherigo.MAINSCREEN)
            end)
    end
end

function Stage11_OnProximity()
    I_Rucksack.Commands.Cmd05.Enabled = false
    Zone10_Origin.Active = false
    Zone11.Display = true
end

function Stage11_Referenz_OnProximity()
    if Wherigo.VectorToPoint(Player.ObjectLocation, Zone11.OriginalPoint):GetValue "m" > 15 then
        msgMitNachricht(getMessage(language, 139),
            function()
                Zone11.Display = true
                Referenz_11.Active = false
                Wherigo.ShowScreen(Wherigo.MAINSCREEN)
            end)
    end
end

function Stage12_OnEnter()
    if outsideZone then
        outsideZone = false
        msgMitNachrichtUndBild(getMessage(language, 150), P_RudolphVersteckt,
            function()
                T_ZuFindendesObjekt.Active = false
                Zone12.Active = false
                Zone13.Active = true
                outsideZone = true
                Wherigo.ShowScreen(Wherigo.MAINSCREEN)
            end)
    end
end

function Stage13_OnEnter()
    if outsideZone then
        outsideZone = false
        msgMitNachrichtUndBild(getMessage(language, 151), P_AlteFrau,
            function()
                V_AlteDame = true
                Wherigo.ShowScreen(Wherigo.DETAILSCREEN, I_Rucksack)
            end)
    end
end

function Stage14_OnProximity()
    if outsideZone then
        outsideZone = false
        msgMitNachricht(getMessage(language, 158),
            function()
                msgMitNachricht(getMessage(language, 159),
                    function()
                        I_Rucksack:MoveTo(nil)
                        I_Schlitten:MoveTo(Zone14)
                        I_UnlockCode:MoveTo(Player)
                        Wherigo.ShowScreen(Wherigo.MAINSCREEN)
                    end)
            end)
    end
end

function I_AkkusBatterien_Cmd01()
    I_AkkusBatterien:MoveTo(nil)
    I_Rucksack.Commands.Cmd03.Enabled = true
    V_Akkus = true
    Rucksack()
end

function I_WFragen_Cmd01()
    Wherigo.GetInput(Q_Tauschen)
end

function I_Fuchs_Cmd01()
    if table.getn(Player.Inventory) > 1 then
        msgMitNachricht(getMessage(language, 119),
            function()
                Wherigo.ShowScreen(Wherigo.MAINSCREEN)
            end)
    else
        I_Fuchs:MoveTo(Player)
        I_Fuchs.Commands.Cmd01.Enabled = false
        I_Fuchs.Commands.Cmd02.Enabled = true
        Wherigo.ShowScreen(Wherigo.MAINSCREEN)
    end
end

function I_Fuchs_Cmd02()
    if Zone09_Ufer1:Contains(Player) then
        I_Fuchs:MoveTo(Zone09_Ufer1)
        I_Fuchs.Commands.Cmd01.Enabled = true
        I_Fuchs.Commands.Cmd02.Enabled = false
    elseif Zone09_Ufer2:Contains(Player) then
        I_Fuchs:MoveTo(Zone09_Ufer2)
        I_Fuchs.Commands.Cmd01.Enabled = true
        I_Fuchs.Commands.Cmd02.Enabled = false
    else
        msgMitNachricht(getMessage(language, 118),
            function()
                Wherigo.ShowScreen(Wherigo.MAINSCREEN)
            end)
    end
    HaseKarotteFuchs()
end

function I_GPS_Cmd01()
    I_GPS:MoveTo(nil)
    I_Rucksack.Commands.Cmd05.Enabled = true
    V_GPS = true
    Rucksack()
end

function I_Hase_Cmd01()
    if table.getn(Player.Inventory) > 1 then
        msgMitNachricht(getMessage(language, 119),
            function()
                Wherigo.ShowScreen(Wherigo.MAINSCREEN)
            end)
    else
        I_Hase:MoveTo(Player)
        I_Hase.Commands.Cmd01.Enabled = false
        I_Hase.Commands.Cmd02.Enabled = true
        Wherigo.ShowScreen(Wherigo.MAINSCREEN)
    end
end

function I_Hase_Cmd02()
    if Zone09_Ufer1:Contains(Player) then
        I_Hase:MoveTo(Zone09_Ufer1)
        I_Hase.Commands.Cmd01.Enabled = true
        I_Hase.Commands.Cmd02.Enabled = false
    elseif Zone09_Ufer2:Contains(Player) then
        I_Hase:MoveTo(Zone09_Ufer2)
        I_Hase.Commands.Cmd01.Enabled = true
        I_Hase.Commands.Cmd02.Enabled = false
    else
        msgMitNachricht(getMessage(language, 118),
            function()
                Wherigo.ShowScreen(Wherigo.MAINSCREEN)
            end)
    end
    HaseKarotteFuchs()
end

function I_Karotte_Cmd01()
    if table.getn(Player.Inventory) > 1 then
        msgMitNachricht(getMessage(language, 119),
            function()
                Wherigo.ShowScreen(Wherigo.MAINSCREEN)
            end)
    else
        I_Karotten:MoveTo(Player)
        I_Karotten.Commands.Cmd01.Enabled = false
        I_Karotten.Commands.Cmd02.Enabled = true
        Wherigo.ShowScreen(Wherigo.MAINSCREEN)
    end
end

function I_Karotte_Cmd02()
    if Zone09_Ufer1:Contains(Player) then
        I_Karotten:MoveTo(Zone09_Ufer1)
        I_Karotten.Commands.Cmd01.Enabled = true
        I_Karotten.Commands.Cmd02.Enabled = false
    elseif Zone09_Ufer2:Contains(Player) then
        I_Karotten:MoveTo(Zone09_Ufer2)
        I_Karotten.Commands.Cmd01.Enabled = true
        I_Karotten.Commands.Cmd02.Enabled = false
    else
        msgMitNachricht(getMessage(language, 118),
            function()
                Wherigo.ShowScreen(Wherigo.MAINSCREEN)
            end)
    end
    HaseKarotteFuchs()
end

function I_Plasticktuete_Cmd01()
    I_Plasticktuete:MoveTo(nil)
    I_Rucksack.Commands.Cmd02.Enabled = true
    V_Plastiktuete = true
    Rucksack()
end

function I_Powerriegel_Cmd01()
    I_Powerriegel:MoveTo(nil)
    I_Rucksack.Commands.Cmd01.Enabled = true
    V_Powerriegel = true
    Rucksack()
end

function I_Rucksack_Cmd01()
    if V_Rucksackauswahl then
        msgMitNachricht(getMessage(language, 133),
            function()
                Wherigo.ShowScreen(Wherigo.DETAILSCREEN, I_Rucksack)
            end)
    elseif V_RucksackPowerRiegel then
        msgMitNachricht(getMessage(language, 134),
            function()
                V_RucksackPowerRiegel = false
                V_DieInsel = true
                T_Inselpeilung.Active = true
                I_Rucksack.Commands.Cmd01.Enabled = false
                Zone10.Active = false
                Zone10_Origin.Active = true
                Zone11.Active = true
                outsideZone = true
                Wherigo.ShowScreen(Wherigo.MAINSCREEN)
            end)
    elseif V_DieInsel then
        msgMitNachricht(getMessage(language, 135),
            function()
                Wherigo.ShowScreen(Wherigo.DETAILSCREEN, I_Rucksack)
            end)
    elseif V_AlteDame then
        msgMitNachricht(getMessage(language, 136),
            function()
                Wherigo.ShowScreen(Wherigo.DETAILSCREEN, I_Rucksack)
            end)
    else
        msgMitNachricht(getMessage(language, 137),
            function()
                Wherigo.ShowScreen(Wherigo.MAINSCREEN)
            end)
    end
end

function I_Rucksack_Cmd02()
    if V_Rucksackauswahl then
        msgMitNachricht(getMessage(language, 152),
            function()
                Wherigo.ShowScreen(Wherigo.DETAILSCREEN, I_Rucksack)
            end)
    elseif V_RucksackPowerRiegel then
        msgMitNachricht(getMessage(language, 153),
            function()
                Wherigo.ShowScreen(Wherigo.DETAILSCREEN, I_Rucksack)
            end)
    elseif V_DieInsel then
        msgMitNachricht(getMessage(language, 154),
            function()
                Wherigo.ShowScreen(Wherigo.DETAILSCREEN, I_Rucksack)
            end)
    elseif V_AlteDame then
        msgMitNachricht(getMessage(language, 155) .. Player.Name .. getMessage(language, 156),
            function()
                V_AlteDame = false
                I_Rucksack.Commands.Cmd02.Enabled = false
                Zone13.Active = false
                Zone14.Active = true
                outsideZone = true
                Wherigo.ShowScreen(Wherigo.MAINSCREEN)
            end)
    else
        msgMitNachricht(getMessage(language, 157),
            function()
                Wherigo.ShowScreen(Wherigo.MAINSCREEN)
            end)
    end
end

function I_Rucksack_Cmd03()
    if V_Rucksackauswahl then
        msgMitNachricht(getMessage(language, 161),
            function()
                Wherigo.ShowScreen(Wherigo.DETAILSCREEN, I_Rucksack)
            end)
    elseif V_RucksackPowerRiegel then
        msgMitNachricht(getMessage(language, 162),
            function()
                Wherigo.ShowScreen(Wherigo.DETAILSCREEN, I_Rucksack)
            end)
    elseif V_DieInsel then
        msgMitNachricht(getMessage(language, 163),
            function()
                Wherigo.ShowScreen(Wherigo.DETAILSCREEN, I_Rucksack)
            end)
    elseif V_AlteDame then
        msgMitNachricht(getMessage(language, 164),
            function()
                Wherigo.ShowScreen(Wherigo.DETAILSCREEN, I_Rucksack)
            end)
    else
        msgMitNachricht(getMessage(language, 165),
            function()
                Wherigo.ShowScreen(Wherigo.MAINSCREEN)
            end)
    end
end

function I_Rucksack_Cmd04()
    if V_Rucksackauswahl then
        msgMitNachrichtUndBild(getMessage(language, 91), P_RudolphVerletztVerbunden,
            function()
                V_Rucksackauswahl = false
                I_Rucksack.Commands.Cmd04.Enabled = false
                Zone03.Active = false
                Zone04.Active = true
                outsideZone = true
                Wherigo.ShowScreen(Wherigo.MAINSCREEN)
            end)
    elseif V_RucksackPowerRiegel then
        msgMitNachricht(getMessage(language, 92),
            function()
                Wherigo.ShowScreen(Wherigo.DETAILSCREEN, I_Rucksack)
            end)
    elseif V_DieInsel then
        msgMitNachricht(getMessage(language, 93),
            function()
                Wherigo.ShowScreen(Wherigo.DETAILSCREEN, I_Rucksack)
            end)
    elseif V_AlteDame then
        msgMitNachricht(getMessage(language, 94),
            function()
                Wherigo.ShowScreen(Wherigo.DETAILSCREEN, I_Rucksack)
            end)
    else
        msgMitNachricht(getMessage(language, 95),
            function()
                Wherigo.ShowScreen(Wherigo.MAINSCREEN)
            end)
    end
end

function I_Rucksack_Cmd05()
    if V_Rucksackauswahl then
        msgMitNachricht(getMessage(language, 140),
            function()
                Wherigo.ShowScreen(Wherigo.DETAILSCREEN, I_Rucksack)
            end)
    elseif V_RucksackPowerRiegel then
        msgMitNachricht(getMessage(language, 141),
            function()
                Wherigo.ShowScreen(Wherigo.DETAILSCREEN, I_Rucksack)
            end)
    elseif V_DieInsel then
        Wherigo.GetInput(Q_GPS)
    elseif V_AlteDame then
        msgMitNachricht(getMessage(language, 142),
            function()
                Wherigo.ShowScreen(Wherigo.DETAILSCREEN, I_Rucksack)
            end)
    else
        msgMitNachricht(getMessage(language, 143),
            function()
                Wherigo.ShowScreen(Wherigo.MAINSCREEN)
            end)
    end
end

function I_Rucksack_Cmd06()
    if V_Rucksackauswahl then
        msgMitNachricht(getMessage(language, 166),
            function()
                Wherigo.ShowScreen(Wherigo.DETAILSCREEN, I_Rucksack)
            end)
    elseif V_RucksackPowerRiegel then
        msgMitNachricht(getMessage(language, 167),
            function()
                Wherigo.ShowScreen(Wherigo.DETAILSCREEN, I_Rucksack)
            end)
    elseif V_DieInsel then
        msgMitNachricht(getMessage(language, 168),
            function()
                Wherigo.ShowScreen(Wherigo.DETAILSCREEN, I_Rucksack)
            end)
    elseif V_AlteDame then
        msgMitNachricht(getMessage(language, 169),
            function()
                Wherigo.ShowScreen(Wherigo.DETAILSCREEN, I_Rucksack)
            end)
    else
        msgMitNachricht(getMessage(language, 170),
            function()
                Wherigo.ShowScreen(Wherigo.MAINSCREEN)
            end)
    end
end

function I_Stifte_Cmd01()
    I_Stifte:MoveTo(nil)
    I_Rucksack.Commands.Cmd06.Enabled = true
    V_Stifte = true
    Rucksack()
end

function I_Unlockcode_Cmd01()
    I_UnlockCode.Description = getMessage(language, 160) .. Player.CompletionCode
    I_UnlockCode.Commands.Cmd01.Enabled = false
    Wherigo.ShowScreen(Wherigo.DETAILSCREEN, I_Unlockcode)
end

function I_Verbandstasche_Cmd01()
    I_Verbandstasche:MoveTo(nil)
    I_Rucksack.Commands.Cmd04.Enabled = true
    V_Verbandszeug = true
    Rucksack()
end

function Q_DasTreffen_OnGetInput(answer)
    if answer == "" then
        return
    end
    if answer == "3850" then
        msgMitNachrichtUndBild(getMessage(language, 83), nil,
            function()
                I_DasTreffen:MoveTo(nil)
                I_Stifte:MoveTo(Zone02)
                I_Verbandstasche:MoveTo(Zone02)
                I_AkkusBatterien:MoveTo(Zone02)
                I_GPS:MoveTo(Zone02)
                I_Plasticktuete:MoveTo(Zone02)
                I_Powerriegel:MoveTo(Zone02)
            end)
    else
        msgMitNachricht(getMessage(language, 84),
            function()
                Wherigo.GetInput(Q_DasTreffen)
            end)
    end
end

function Q_Tauschen_OnGetInput(answer)
    if answer == "" then
        return
    end
    answer = tonumber(answer)
    if answer < 911 then
        Antwort_04 = answer
        reihenfolgeTauschen(Antwort_04)
        I_WFragen.Description = "01. " .. Reihenfolge_04[1] .. cr
                .. "02. " .. Reihenfolge_04[2] .. cr
                .. "03. " .. Reihenfolge_04[3] .. cr
                .. "04. " .. Reihenfolge_04[4] .. cr
                .. "05. " .. Reihenfolge_04[5] .. cr
                .. "06. " .. Reihenfolge_04[6] .. cr
                .. "07. " .. Reihenfolge_04[7] .. cr
                .. "08. " .. Reihenfolge_04[8] .. cr
                .. "09. " .. Reihenfolge_04[9] .. cr
                .. "10. " .. Reihenfolge_04[10] .. cr .. cr
                .. "Die 5-W sind noch nicht in der Reihenfolge von 1-5, da musst du noch weiter tauschen."
                .. cr .. cr .. "Bitte denke daran das es um Erste Hilfe Fragen geht, nicht um Feuerwehr oder Polizei."
    elseif answer > 911 then
        msgMitNachricht(getMessage(language, 86),
            function()
                if button == true then
                    Wherigo.GetInput(Q_Tauschen)
                elseif button == true then
                    Wherigo.ShowScreen(Wherigo.MAINSCREEN)
                end
            end)
    end
end

function Q_Tierarztpraxis_OnGetInput(answer)
    if answer == "" then
        return
    end
    if answer == "15" or answer == "Fuenfzehn" or answer == "Fuenfzehn " then
        msgMitNachrichtUndBild(getMessage(language, 97), P_RudolphVerbundenLaufend,
            function()
                I_Tierarztpraxis:MoveTo(nil)
                I_Telephonebox:MoveTo(Z_TelephoneHelper)
                T_TelefonzelleZurueck.Active = true
                Zone05.Active = false
                Z_TelephoneHelper.Active = true
                Zone06.Active = true
                outsideZone = true
                Wherigo.ShowScreen(Wherigo.MAINSCREEN)
            end)
    else
        msgMitNachricht(getMessage(language, 98),
            function()
                Wherigo.GetInput(Q_Tierarztpraxis)
            end)
    end
end

function Q_Telefon_OnGetInput(answer)
    if answer == "" then
        return
    end
    if answer == "367340" or answer == "0911/367340"
            or answer == "0911367340" or answer == "0911 367340" then
        msgMitNachrichtUndBild(getMessage(language, 99), P_RudolphVerletztVerbunden,
            function()
                I_Telefon:MoveTo(nil)
                Zone06.Active = false
                Zone07.Active = true
                outsideZone = true
                Wherigo.ShowScreen(Wherigo.MAINSCREEN)
            end)
    else
        msgMitNachricht(getMessage(language, 100),
            function()
                Wherigo.GetInput(Q_Telefon)
            end)
    end
end

function Q_Weltkugel_OnGetInput(answer)
    if answer == "" then
        return
    end
    if answer == "blau wie der Pazifik!" then
        msgMitNachricht(getMessage(language, 102),
            function()
                I_Weltkugel:MoveTo(nil)
                Zone07.Active = false
                Zone07_Strafe.Active = true
                outsideZone = true
                Wherigo.ShowScreen(Wherigo.MAINSCREEN)
            end)
    elseif answer == "rot wie Rudolphs Nase!" then
        msgMitNachrichtUndBild(getMessage(language, 103), P_RudolphPortraet,
            function()
                I_Weltkugel:MoveTo(nil)
                Zone07.Active = false
                Zone08.Active = true
                outsideZone = true
                Wherigo.ShowScreen(Wherigo.MAINSCREEN)
            end)
    elseif answer == "gelb wie ein Sonnenstrahl!" then
        msgMitNachricht(getMessage(language, 104),
            function()
                I_Weltkugel:MoveTo(nil)
                Zone07.Active = false
                Zone07_Strafe.Active = true
                outsideZone = true
                Wherigo.ShowScreen(Wherigo.MAINSCREEN)
            end)
    elseif answer == "Hauptmenue" then
        Wherigo.ShowScreen(Wherigo.MAINSCREEN)
    else
        msgMitNachricht(getMessage(language, 105),
            function()
                Wherigo.GetInput(Q_Weltkugel)
            end)
    end
end

function Q_SeineFreunde_OnGetInput(answer)
    if answer == "" then
        return
    end
    if answer == "A" then
        msgMitNachricht(getMessage(language, 107),
            function()
                Wherigo.GetInput(Q_SeineFreunde)
            end)
    elseif answer == "B" then
        msgMitNachrichtUndBild(getMessage(language, 108), P_RudolphFreunde,
            function()
                Zone08.Active = false
                Zone09_Hase.Active = true
                outsideZone = true
                I_SeineFreunde:MoveTo(nil)
                Wherigo.ShowScreen(Wherigo.MAINSCREEN)
            end)
    elseif answer == "C" then
        msgMitNachricht(getMessage(language, 109),
            function()
                Wherigo.GetInput(Q_SeineFreunde)
            end)
    elseif answer == "D" then
        Wherigo.ShowScreen(Wherigo.MAINSCREEN)
    else
        msgMitNachrichtUndButtons(getMessage(language, 110), getMessage(language, 111), getMessage(language, 112),
            function()
                Wherigo.GetInput(Q_SeineFreunde)
            end,
            function ()
                Wherigo.ShowScreen(Wherigo.MAINSCREEN)
            end)
    end
end

function Q_Suess_OnGetInput(answer)
    if answer == "" then
        return
    end
    if answer == "A" then
        msgMitNachricht(getMessage(language, 121),
            function()
                Wherigo.GetInput(Q_Suess)
            end)
    elseif answer == "B" then
        msgMitNachricht(getMessage(language, 127),
            function()
                Wherigo.GetInput(Q_Suess)
            end)
    elseif answer == "C" then
        msgMitNachricht(getMessage(language, 122),
            function()
                I_Suess:MoveTo(nil)
                V_RucksackPowerRiegel = true
                Wherigo.ShowScreen(Wherigo.DETAILSCREEN, I_Rucksack)
            end)
    elseif answer == "D" then
        msgMitNachricht(getMessage(language, 123),
            function()
                Wherigo.ShowScreen(Wherigo.MAINSCREEN)
            end)
    else
        msgMitNachrichtUndButtons(getMessage(language, 124), getMessage(language, 125), getMessage(language, 126),
            function()
                Wherigo.GetInput(Q_Suess)
            end,
            function()
                Wherigo.ShowScreen(Wherigo.MAINSCREEN)
            end)
    end
end

function Q_GPS_OnGetInput(answer)
    if answer == "" then
        return
    end
    if answer == "Meter" then
        Wherigo.GetInput(Q_Meter)
    elseif answer == "Grad" then
        Wherigo.GetInput(Q_Grad)
    elseif answer == "Bestaetigen" then
        local point = Player.ObjectLocation
        point = Wherigo.TranslatePoint(point, Wherigo.Distance(Meter_11, 'm'), Grad_11)
        Referenz_11.Points = { point, point, point, point }
        Referenz_11.OriginalPoint = point
        Referenz_11.Active = false
        Referenz_11.Active = true
        Wherigo.ShowScreen(Wherigo.MAINSCREEN)
    elseif answer == "Zurueck" then
        Wherigo.ShowScreen(Wherigo.MAINSCREEN)
    else
        msgMitNachrichtUndButtons(getMessage(language, 144), getMessage(language, 145), getMessage(language, 146),
            function()
                Wherigo.GetInput(Q_GPS)
            end,
            function()
                Wherigo.ShowScreen(Wherigo.MAINSCREEN)
            end)
    end
end

function Q_Grad_OnGetInput(answer)
    if answer == "" then
        return
    end
    Grad_11 = answer
    Q_GPS.Text = getMessage(language, 147) .. Grad_11 .. getMessage(language, 148)
            .. Meter_11 .. getMessage(language, 149)
    Wherigo.GetInput(Q_GPS)
end

function Q_Meter_OnGetInput(answer)
    if answer == "" then
        return
    end
    Meter_11 = answer
    Q_GPS.Text = getMessage(language, 147) .. Grad_11 .. getMessage(language, 148)
            .. Meter_11 .. getMessage(language, 149)
    Wherigo.GetInput(Q_GPS)
end

function Rucksack()
    if V_Verbandszeug and V_GPS and V_Akkus and V_Plastiktuete
            and V_Stifte and V_Powerriegel then
        msgMitNachrichtUndBild(getMessage(language, 88), P_Sprechblase,
            function()
                msgMitNachricht(getMessage(language, 89),
                    function()
                        msgMitNachrichtUndBild(getMessage(language, 90), P_RudolphVerletzt,
                            function()
                                Zone02.Active = false
                                Zone03.Active = true
                                outsideZone = true
                                Wherigo.ShowScreen(Wherigo.MAINSCREEN)
                            end)
                    end)
            end)
    else
        Wherigo.ShowScreen(Wherigo.MAINSCREEN)
    end
end

function Notruf()
    msgMitNachricht(getMessage(language, 87),
        function()
            T_Die5W.Compete = true
            T_Die5W.Active = false
            I_WFragen:MoveTo(nil)
            I_Rucksack:MoveTo(Player)
            Zone04.Active = false
            Zone05.Active = true
            outsideZone = true
            Wherigo.ShowScreen(Wherigo.MAINSCREEN)
        end)
end

function HaseKarotteFuchs()
    if Zone09_Ufer2:Contains(I_Fuchs)
            and Zone09_Ufer2:Contains(I_Karotten)
            and Zone09_Ufer2:Contains(I_Hase)
            and Zone09_Ufer2:Contains(Player) then
        msgMitNachrichtUndBild(getMessage(language, 115), P_HaseFuchsKarotten,
            function()
                I_Fuchs:MoveTo(nil)
                I_Karotten:MoveTo(nil)
                I_Hase:MoveTo(nil)
                T_HaseKarotteFuchs.Complete = true
                T_HaseKarotteFuchs.Active = false
                Zone09_Ufer1.Active = false
                Zone09_Ufer2.Active = false
                Zone10.Active = true
                Wherigo.ShowScreen(Wherigo.MAINSCREEN)
            end)
    elseif (Zone09_Ufer1:Contains(I_Hase)
            and Zone09_Ufer1:Contains(I_Karotten)
            and not Zone09_Ufer1:Contains(Player))
            or (Zone09_Ufer2:Contains(I_Hase)
            and Zone09_Ufer2:Contains(I_Karotten)
            and not Zone09_Ufer2:Contains(Player)) then
        msgMitNachricht(getMessage(language, 116),
            function()
                ResetHaseKarotteFuchs()
                Wherigo.ShowScreen(Wherigo.MAINSCREEN)
            end)
    elseif (Zone09_Ufer1:Contains(I_Fuchs)
            and Zone09_Ufer1:Contains(I_Hase)
            and not Zone09_Ufer1:Contains(Player))
            or (Zone09_Ufer2:Contains(I_Fuchs)
            and Zone09_Ufer2:Contains(I_Hase)
            and not Zone09_Ufer2:Contains(Player)) then
        msgMitNachricht(getMessage(language, 117,
            function()
                ResetHaseKarotteFuchs()
                Wherigo.ShowScreen(Wherigo.MAINSCREEN)
            end))
    else
        Wherigo.ShowScreen(Wherigo.MAINSCREEN)
    end
end

function ResetHaseKarotteFuchs()
    I_Fuchs:MoveTo(Zone09_Ufer1)
    I_Fuchs.Commands.Cmd01.Enabled = true
    I_Fuchs.Commands.Cmd02.Enabled = false
    I_Karotten:MoveTo(Zone09_Ufer1)
    I_Karotten.Commands.Cmd01.Enabled = true
    I_Karotten.Commands.Cmd02.Enabled = false
    I_Hase:MoveTo(Zone09_Ufer1)
    I_Hase.Commands.Cmd01.Enabled = true
    I_Hase.Commands.Cmd02.Enabled = false
end

Reihenfolge_04 = {
    "Wie ist es passiert?",
    "Wo ist es passiert?",
    "Wem ist es passiert?",
    "Was ist passiert?",
    "Wer ist Schuld?",
    "Welche Verletzungen gibt es?",
    "Warten auf Fragen!",
    "Warten auf die Polizei!",
    "Wie viele Verletzte gibt es?",
    "Wann ist es passiert?"
}
KorrektePosition_04 = {
    "Wo ist es passiert?",
    "Was ist passiert?",
    "Wie viele Verletzte gibt es?",
    "Welche Verletzungen gibt es?",
    "Warten auf Fragen!"
}

function reihenfolgeTauschen(ersteZahl)
    if Antwort_04 == 101 or Antwort_04 == 110 then
        tauschen(10, 1)
    elseif Antwort_04 == 102 or Antwort_04 == 210 then
        tauschen(10, 2)
    elseif Antwort_04 == 103 or Antwort_04 == 310 then
        tauschen(10, 3)
    elseif Antwort_04 == 104 or Antwort_04 == 410 then
        tauschen(10, 4)
    elseif Antwort_04 == 105 or Antwort_04 == 510 then
        tauschen(10, 5)
    elseif Antwort_04 == 106 or Antwort_04 == 610 then
        tauschen(10, 6)
    elseif Antwort_04 == 107 or Antwort_04 == 710 then
        tauschen(10, 7)
    elseif Antwort_04 == 108 or Antwort_04 == 810 then
        tauschen(10, 8)
    elseif Antwort_04 == 109 or Antwort_04 == 910 then
        tauschen(10, 9)
    elseif Antwort_04 == 12 or Antwort_04 == 21 then
        tauschen(1, 2)
    elseif Antwort_04 == 13 or Antwort_04 == 31 then
        tauschen(1, 3)
    elseif Antwort_04 == 14 or Antwort_04 == 41 then
        tauschen(1, 4)
    elseif Antwort_04 == 15 or Antwort_04 == 51 then
        tauschen(1, 5)
    elseif Antwort_04 == 16 or Antwort_04 == 61 then
        tauschen(1, 6)
    elseif Antwort_04 == 17 or Antwort_04 == 71 then
        tauschen(1, 7)
    elseif Antwort_04 == 18 or Antwort_04 == 81 then
        tauschen(1, 8)
    elseif Antwort_04 == 19 or Antwort_04 == 91 then
        tauschen(1, 9)
    elseif Antwort_04 == 23 or Antwort_04 == 32 then
        tauschen(2, 3)
    elseif Antwort_04 == 24 or Antwort_04 == 42 then
        tauschen(2, 4)
    elseif Antwort_04 == 25 or Antwort_04 == 52 then
        tauschen(2, 5)
    elseif Antwort_04 == 26 or Antwort_04 == 62 then
        tauschen(2, 6)
    elseif Antwort_04 == 27 or Antwort_04 == 72 then
        tauschen(2, 7)
    elseif Antwort_04 == 28 or Antwort_04 == 82 then
        tauschen(2, 8)
    elseif Antwort_04 == 29 or Antwort_04 == 92 then
        tauschen(2, 9)
    elseif Antwort_04 == 34 or Antwort_04 == 43 then
        tauschen(3, 4)
    elseif Antwort_04 == 35 or Antwort_04 == 53 then
        tauschen(3, 5)
    elseif Antwort_04 == 36 or Antwort_04 == 63 then
        tauschen(3, 6)
    elseif Antwort_04 == 37 or Antwort_04 == 73 then
        tauschen(3, 7)
    elseif Antwort_04 == 38 or Antwort_04 == 83 then
        tauschen(3, 8)
    elseif Antwort_04 == 39 or Antwort_04 == 93 then
        tauschen(3, 9)
    elseif Antwort_04 == 45 or Antwort_04 == 54 then
        tauschen(4, 5)
    elseif Antwort_04 == 46 or Antwort_04 == 64 then
        tauschen(4, 6)
    elseif Antwort_04 == 47 or Antwort_04 == 74 then
        tauschen(4, 7)
    elseif Antwort_04 == 48 or Antwort_04 == 84 then
        tauschen(4, 8)
    elseif Antwort_04 == 49 or Antwort_04 == 94 then
        tauschen(4, 9)
    elseif Antwort_04 == 56 or Antwort_04 == 65 then
        tauschen(5, 6)
    elseif Antwort_04 == 57 or Antwort_04 == 75 then
        tauschen(5, 7)
    elseif Antwort_04 == 58 or Antwort_04 == 85 then
        tauschen(5, 8)
    elseif Antwort_04 == 59 or Antwort_04 == 95 then
        tauschen(5, 9)
    elseif Antwort_04 == 67 or Antwort_04 == 76 then
        tauschen(6, 7)
    elseif Antwort_04 == 68 or Antwort_04 == 86 then
        tauschen(6, 8)
    elseif Antwort_04 == 69 or Antwort_04 == 96 then
        tauschen(6, 9)
    elseif Antwort_04 == 78 or Antwort_04 == 87 then
        tauschen(7, 8)
    elseif Antwort_04 == 79 or Antwort_04 == 97 then
        tauschen(7, 9)
    elseif Antwort_04 == 89 or Antwort_04 == 98 then
        tauschen(8, 9)
    end
end

function tauschen(ersteZahl, zweiteZahl)
    local Text1 = Reihenfolge_04[ersteZahl]
    local Text2 = Reihenfolge_04[zweiteZahl]
    Reihenfolge_04[ersteZahl] = Text2
    Reihenfolge_04[zweiteZahl] = Text1
    if Reihenfolge_04[1] == KorrektePosition_04[1] and Reihenfolge_04[2] == KorrektePosition_04[2] and Reihenfolge_04[3] == KorrektePosition_04[3] and Reihenfolge_04[4] == KorrektePosition_04[4] and Reihenfolge_04[5] == KorrektePosition_04[5] then
        --Funktion aufrufen mit Textfeld
        Notruf()
    else
        --Nix
    end
end
