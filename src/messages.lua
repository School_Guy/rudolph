messages = {}
    messages ["de"] = {}
        messages ["de"] [1] = "01 Start"
		messages ["de"] [2] = "02 Das Treffen"
		messages ["de"] [3] = "03 Laufversuch"
		messages ["de"] [4] = "04 Notruf"
		messages ["de"] [5] = "05 Tierarztpraxis"
		messages ["de"] [6] = "06 Telefon"
		messages ["de"] [7] = "07 Strafzone"
		messages ["de"] [8] = "07 Weltkugel"
		messages ["de"] [9] = "08 seine Freunde"
		messages ["de"] [10] = "09 Hase"
		messages ["de"] [11] = "09 Ufer 1"
		messages ["de"] [12] = "09 Ufer 2"
		messages ["de"] [13] = "10 Suess"
		messages ["de"] [14] = "11 Die Insel"
		messages ["de"] [15] = "11 Die Insel Referenz"
		messages ["de"] [16] = "12 versteckte Zone"
		messages ["de"] [17] = "13 Die alte Dame"
		messages ["de"] [18] = "14 Final"
		messages ["de"] [19] = "Akkus und Batterien"
		messages ["de"] [20] = "Die W-Fragen"
		messages ["de"] [21] = "Fuchs"
		messages ["de"] [22] = "GPS"
		messages ["de"] [23] = "Hase"
		messages ["de"] [24] = "Karotten"
		messages ["de"] [25] = "Plastiktuete"
		messages ["de"] [26] = "Powerriegel"
		messages ["de"] [27] = "Rucksack"
		messages ["de"] [28] = "Schlitten"
		messages ["de"] [29] = "Stifte"
		messages ["de"] [30] = "Unlock-Code"
		messages ["de"] [31] = "Verbandstasche"
		messages ["de"] [32] = "Beantworten"
		messages ["de"] [33] = "Einpacken"
		messages ["de"] [34] = "01. Wie ist es passiert?"
		messages ["de"] [35] = "02. Wo ist es passiert?"
		messages ["de"] [36] = "03. Wem ist es passiert?"
		messages ["de"] [37] = "04. Was ist passiert?"
		messages ["de"] [38] = "05. Wer ist Schuld?"
		messages ["de"] [39] = "06. Welche Verletzungen gibt es?"
		messages ["de"] [40] = "07. Warten auf Fragen!"
		messages ["de"] [41] = "08. Warten auf die Polizei!"
		messages ["de"] [42] = "09. Wie viele Verletzte gibt es?"
		messages ["de"] [43] = "10. Wann ist es passiert?"
		messages ["de"] [44] = "Einladen"
		messages ["de"] [45] = "Ausladen"
		messages ["de"] [46] = "Anzeigen"
		messages ["de"] [47] = "Die 5-W"
		messages ["de"] [48] = "Bringe die 5-W in die richtige Reihenfolge!"
		messages ["de"] [49] = "Gehe zurueck zur Telefonzelle!"
		messages ["de"] [50] = "P.S.: Ihr wart dort schon!"
		messages ["de"] [51] = "Hase-Karotten-Fuchs"
		messages ["de"] [52] = [[Folgende Regeln fuer dieses Raetsel:
- Es darf zu keinem Zeitpunkt Hase und Fuchs allein sein (waehrend ihr bei ihnen seit koennt ihr den Fuchs hindern den Hasen zu fressen).
- Es darf zu keinem Zeitpunkt Hase und die Karotten allein sein (waehrend ihr bei ihnen seid koennt ihr den Hasen daran hindern die Karotten zu fressen).
- Die Aufgabe ist erfuellt, wenn Fuchs, Hase, Karotten und ihr auf der anderen Seite seid (Ufer 2).
- Man kann immer nur eine Sache gleichzeitig transportieren.
- Wenn eine dieser Regeln nicht eingehalten wird, wird alles in die Anfangsposition zurueckgestellt.]]
		messages ["de"] [53] = "Inselpeilung"
		messages ["de"] [54] = "Gehe 79 Meter Richtung 237 Grad."
		messages ["de"] [55] = "Zu findendes Objekt!"
		messages ["de"] [56] = "Hier steht meine gut wettergeschuetzte Geocachingkiste. Fuer Muggel habe ich natuerlich ein gefaktes Zylinderschloss angebracht. Geocacher wissen natuerlich mit welchem vierstelligem Zahlencode sie die Kiste oeffnen koennen."
		messages ["de"] [57] = "3850"
		messages ["de"] [58] = [[Welche Nummer magst du verschieben?

Bsp: 25 bedeutet, das Position 02 zu 05 wird und 05 wird zu 02.]]
		messages ["de"] [59] = "Wie viele Katzen, Hunde und heilende Schlangen warten hier aussen insgesamt schon auf Euch?"
		messages ["de"] [60] = [[Rudolph ruft bei seinen Freunden an, aber es hebt niemand ab. Aber der Anrufbeantworter geht ran. Er hinterlaesst eine Nachricht. "... und wenn ihr mich erreichen wollt, dann ruft zurueck unter der 0911/367340"]]
		messages ["de"] [61] = "Die Klettergriffe haben welche Farbe: (bei Falschantwort muss 100 m gelaufen werden)"
		messages ["de"] [62] = "blau wie der Pazifik!"
		messages ["de"] [63] = "rot wie Rudolphs Nase!"
		messages ["de"] [64] = "rot wie Rudolphs Nase!"
		messages ["de"] [65] = "gelb wie ein Sonnenstrahl!"
		messages ["de"] [66] = "Hauptmenue"
		messages ["de"] [67] = [[So gerne moechte ich ihm helfen und ich weiss, dass ihre Namen im Lied vorkommen. Wie heissen die anderen Rentiere noch?

A: Dasher, Dancer, Proxter, Meteor, Amor, Cloud and Blitzen
B: Dasher, Dancer, Prancer , Vixen, Comet, Cupid, Donner and Blitzen
C: Washer, Dancer, Prancer, Desire, Wisher, Thanker, Donner and Blitzen
D: Hauptmenue]]
		messages ["de"] [68] = "A"
		messages ["de"] [69] = "B"
		messages ["de"] [70] = "C"
		messages ["de"] [71] = "D"
		messages ["de"] [72] = [[Ich schaue mich um, denn dem sympathischen Kerl kann ich nichts abschlagen.

A: Ich jogge zum naechsten Geschaeft
B: Ich zuecke mein Telefon und rufe einen TJ an, der mit einer Zuckertuete herkommen soll.
C: Da war doch etwas in meinem Rucksack
D: Hauptmenue]]
		messages ["de"] [73] = "Gebe die Nummer deines TJs ein!"
		messages ["de"] [74] = "Wo magst du hin? (00 Grad; 00 Meter)"
		messages ["de"] [75] = "Meter"
		messages ["de"] [76] = "Grad"
		messages ["de"] [77] = "Bestaetigen"
		messages ["de"] [78] = "Zurueck"
		messages ["de"] [79] = "Wie viele Grad magst du peilen?"
		messages ["de"] [80] = "Wie viele Meter magst du peilen?"
		messages ["de"] [81] = [[Ich starte meinen Cachetag "HIER". Wie immer ueberpruefe ich auf dem Boden, dass ich auch richtig bin.

Es nicht mehr weit bis Weihnachten und ich habe einen freien Tag. So einen Tag muss man einfach zum Cachen nutzen! Also schnappe ich meinen Rucksack und trete vors Haus.
In meinem Kopf dreht sich ein Weihnachtsohrwurm, den ich staendig vor mich hinsumme. Es geht um einen "Rudolph", aber ich kenne natuerlich den Text nicht vollstaendig.
Da ich meine Familie als Muggel bezeichnen muss, verstecke ich meine Ausruestung in einem Schrank ausserhalb der Wohnung. Also gehe ich jetzt erstmal meinen Rucksack fuellen.]]
		messages ["de"] [82] = "Da das Humpeln nicht besser wird, buecke ich mich und untersuche das verletzte Bein. Natuerlich bin ich kein Rentierarzt, aber auch ich sehe, dass man die Wunde ein bisschen versorgen sollte."
		messages ["de"] [83] = "Dann nehme ich die Sachen aus der Kiste heute mal alle mit und packe sie in meinen Rucksack."
		messages ["de"] [84] = "Deinen Augen geht es scheinbar wirklich nicht gut!"
		messages ["de"] [85] = [[Jetzt musst ich einen Notruf absetzen.

Ich waehle die 112.
Die genaue Notfall/Unfallmeldung ist fuer die Rettungskraefte sehr wichtig, daran kann ich mich aus dem Erste-Hilfe-Kurs erinnern, da sie sich anhand der Informationen genau auf den Notfall/Unfall vorbereiten koennen.

Dazu muss ich die 5 W der Ersten Hilfe in die richtige Reihenfolge bringen (Feuerwehr und Polizei haben andere Fragen).]]
		messages ["de"] [86] = "Bitte ueberpruefe ob du dich verschrieben hast oder entscheide dich ins Hauptmenue zurueck zu gehen."
		messages ["de"] [87] = [[Das ist richtig!

Jetzt haben Rudolph und ich ein kleines Problem. Die Notrufleitstelle hat sich bei Rentieren fuer nicht zustaendig erklaert. Aber immerhin haben sie mir die Addresse einer nahen Tierarztpraxis gegeben. Rudolph und ich humpeln dorthin.]]
		messages ["de"] [88] = [[Von hinten hoere ich eine leise Stimme:

"Hallo, kannst du mir helfen?"

Im Halbdunkeln drehe ich mich um und schaue, wo die Stimme herkommt. Ich reibe mir sicherheitshalber die Augen. Ich zwinkere. Ich kneife mich. Ich gehe einen Schritt vor und zurueck.]]
		messages ["de"] [89] = [["Entschuldigung! Ich braeuchte Hilfe!"

Also gut! Die Augen scheinen zu funktionieren und trotzdem glaube ich nicht, das da neben mir in der Ecke ein Rentier steht. Besonderes Kennzeichen: die Nase ist leuchtend rot!
Erneut setzt es zum Sprechen an:

"Ich bin ein Weihnachtsrentier und deshalb kann ich auch sprechen. Ich helfe dem Weihnachtsmann in der Weihnachtsnacht, den Kindern ihre Geschenke zu bringen. Das ist ein grossartiger Job! Aber ich hatte einen Unfall und kann mich nicht an alles erinnern."

Es fuegt dann hinzu:

"Danke, dass Du, mir helfen willst. Ich habe Probleme mit meinem Bein."

Tatsaechlich steht das Rentier auf nur drei Beinen. Das vierte Bein blutet leicht. Wir versuchen ob er laufen kann.]]
		messages ["de"] [90] = ""
		messages ["de"] [91] = "Nachdem ich Rudolph das Bein verbunden habe, geht es ihm aber nicht viel besser und ich will lieber professionelle Hilfe rufen."
		messages ["de"] [92] = "Rudolph braucht etwas Energie, die wird er hiervon sicherlich bekommen!"
		messages ["de"] [93] = "Das Teil hilft mir jetzt wirklich nicht um den Weg zu finden!"
		messages ["de"] [94] = "Das hilft mir jetzt nun wirklich nicht!"
		messages ["de"] [95] = "Es gibt gerade hiermit nichts zu tun!"
		messages ["de"] [96] = "Jetzt stehen wir vor einer Tierarztpraxis und klingeln Sturm. "
		messages ["de"] [97] = [[Natuerlich werde ich auf Rudolph warten, denn ich moechte gerne mehr ueber diesen kleinen Kerl erfahren.
Rudolph wurde nun offensichtlich professionell verarztet und als er aus der Praxis herauskommt, bedankt er sich sehr lieb fuer die Erste Hilfe, die ich ihm geleistet habe.
Das kleine Rentier moechte nun gerne seine Freunde anrufen.

Wir waren doch gerade an einem Muenzfernsprechautomaten?]]
		messages ["de"] [98] = [[Bitte schau nochmal nach. Es zaehlen keine Raubkatzen und ein Standortwechsel verfaelscht das Ergebnis.]]
		messages ["de"] [99] = "Er ist ganz ungeduldig und will moegliche Rueckrufe nicht abwarten. Daher will er auch selbst nach ihnen suchen und geht weiter in den Park hinein."
		messages ["de"] [100] = "Unter dieser Nummer werden Rudolphs Freunde keinen Anschluss finden! Bitte sprich die Nachricht nochmal auf!"
		messages ["de"] [101] = "Unterwegs kommen wir an einer Weltkugel vorbei. Rudolph schaut ganz freudig und erzaehlt mir, welche Routenplanung er dieses Jahr Weihnachten sich zurechtgelegt hat. Den dichtesten Nebel erwartet er nach der Langzeitwettervorhersage in Neufundland."
		messages ["de"] [102] = "Das ist nicht richtig, laufe 100 m, komm zurueck und schau dann noch mal genau hin!"
		messages ["de"] [103] = "Ganz genau! Weiter geht es nun!"
		messages ["de"] [104] = "Bist Du Dir sicher, dass Du an der richtigen Stelle stehst? Laufe 100 m, komm zurueck und schau dann noch mal genau hin!"
		messages ["de"] [105] = "Ganz ganz falsche Antwort!"
		messages ["de"] [106] = "In der Zwischenzeit erzaehlt mir Rudolph von seinen Freunden, den anderen Rentieren, mit denen er so viel Spass hatte. Aber von seinem Sturz hat er wohl tatsaechlich eine kleine Amnesie zurueckbehalten, so dass er sich nicht mehr an alle ihre Namen erinnert. Wenn er sie richtig rufen kann, dann koennten sie ihn vielleicht finden."
		messages ["de"] [107] = "Nein, da bleibt Rudoph wohl alleine, so heissen die Rentiere nicht!"
		messages ["de"] [108] = [[Richtig! Die Namen sind: Dasher, Dancer, Prancer , Vixen, Comet, Cupid, Donner und Blitzen.

Dann kann die Suche ja weiter gehen]]
		messages ["de"] [109] = "Nein, da bleibt Rudoph wohl alleine, so heissen die Rentiere nicht!"
		messages ["de"] [110] = "Du hast etwas unvorhergesehenes gemacht, indem du keine der gewaehlten Antworten gewaehlt hast, wo magst du nun hin?"
		messages ["de"] [111] = "Frage"
		messages ["de"] [112] = "Hauptmenue"
		messages ["de"] [113] = [[Ich bin ein bisschen verbluefft, als Rudolph von einem Hasen angesprochen wird und schlucke Bemerkungen in der Richtung von "Ostern und Weihnachten fallen heute wohl auf einen Tag" herunter, denn der Schneehase sieht sehr freundlich aus.

"Gehoert ihr zu den Rentieren, die ich gerade gesprochen habe?
Wenn ihr uns helft, ueber das Wasser zu kommen, dann helfe ich euch bei der Suche nach den anderen Rentieren weiter."]]
		messages ["de"] [114] = [[Er hat ein Problem. Er moechte gerne mit seinem Proviant auf die andere Seite des Wassers, aber der Fuchs hier auf der anderen Wegseite auch. Dabei muss der Proviant aber, trotz seines grossen Hungers, fuer den Rest der Familie unangetastet bleiben.
Ich tueftele einen Plan aus, wie wir alle drei Parteien sicher ueber das Wasser bringen kann. Ich kann dabei jeweils nur ein Tier/Gemuese auf einmal transportieren. Und darf diejenigen, die sich nicht vertragen, nicht miteinander auf einer Seite lassen.]]
		messages ["de"] [115] = "Das hast Du sehr gut gemacht! Dann koennt ihr ja jetzt voran schreiten!"
		messages ["de"] [116] = "Tja, da hat wohl der Hase die Karotten gefressen! Also alles zurueck auf Anfang!"
		messages ["de"] [117] = "Tja, da hat wohl der Fuchs den Hasen gefressen! Also alles zurueck auf Anfang!"
		messages ["de"] [118] = "Du bist in keiner Zone, bitte begebe dich in eine der beiden Zonen!"
		messages ["de"] [119] = "Du kannst nur eine Sache gleichzeitig bei dir haben!"
		messages ["de"] [120] = [[Nun ist Rudolph wieder fit. Das Bein sieht schon wieder recht gut aus. Aber seine Nase glaenzt eher matt und ist trocken. Er meldet sich auch gleich zu Wort. "Die Verletzung war kraftraubend und der Schock des Absturzes war hoch. Jetzt habe ich richtig Hunger! Kannst Du mir bitte etwas zum Essen besorgen? Bitte achte auf einen moeglichst hohen Zuckergehalt. Das ist fuer Menschen nicht guenstig, aber Weihnachtsrentiere brauchen das!"]]
		messages ["de"] [121] = "Gute Idee, aber dauert zu lange. Rudolph muss sein Training schnell wieder aufnehmen."
		messages ["de"] [122] = "Da war doch etwas in meinem Rucksack"
		messages ["de"] [123] = "Die Frage muss beantwortet werden um die naechste Aktion durchzufueren, kehre also wieder hierhin zurueck wenn du weiterspielen magst."
		messages ["de"] [124] = "Du hast etwas unvorhergesehenes gemacht! Wo magst du nun hin?"
		messages ["de"] [125] = "Zur Frage"
		messages ["de"] [126] = "Hauptmenue"
        messages ["de"] [127] = "Du hattest wohl einen Tagtraum? Welches Telefon denn? Wir mussten doch sogar zur Telefonzelle um für Rudolph Hilfe zu organisieren. Und die Zuckertuete ist nun gerade auch nicht noetig, die naechste Mahlzeit gibt es schließlich nach dem du wieder daheim bist!"
		messages ["de"] [133] = "Das Teil hilft mir nicht. Da muss ich noch weitersuchen."
		messages ["de"] [134] = [[Rudolph ist uebergluecklich, dass du ihm etwas Staerkung verschaffen konntest!
Rudolph hat nun Lust zum Spielen. "Hey," meint er "in ca. 79 m Entfernung und in etwa 237 Grad befindet sich eine Insel. Wer findet sie zuerst?!"

(Anmerkungen d. Redaktion:
1. Ihr habt eine Moeglichkeit im Wherigo zu peilen.
2. Bitte bleibt auf offiziellen Wegen, denn uebers Wasser laufen kann nur Jesus.)]]
		messages ["de"] [135] = "Das Teil hilft mir jetzt wirklich nicht um den Weg zu finden"
		messages ["de"] [136] = "Das hilft mir jetzt nun wirklich nicht!"
		messages ["de"] [137] = "Es gibt gerade hiermit nichts zu tun!"
		messages ["de"] [138] = "Rudolph findet es ganz toll, dass Du die Insel gefunden hast. Aber jetzt will er Verstecken spielen. Auf diesem Bild siehst Du, wo er sich versteckt hat. Such ihn!"
		messages ["de"] [139] = "Da du wohl nicht mit einem GPS eine Peilung durchfuehren kannst oder alsche Daten eingegeben hast, schenkt dir Rudolph nun zu Weihnachten die sichtbare Zone der Insel."
		messages ["de"] [140] = "Das Teil hilft mir nicht. Da muss ich noch weitersuchen."
		messages ["de"] [141] = "Rudolph braucht etwas Energie, die wird er hiervon sicherlich nicht bekommen!"
		messages ["de"] [142] = "Das hilft mir jetzt nun wirklich nicht!"
		messages ["de"] [143] = "Es gibt gerade hiermit nichts zu tun!"
		messages ["de"] [144] = "Du hast etwas unvorhersehbares getan, du hast keine der verfuegbaren Antwortmoeglichkeiten gewaehlt! Wo magst du nun hin?"
		messages ["de"] [145] = "Frage"
		messages ["de"] [146] = "Hauptmenue"
		messages ["de"] [147] = "Wo magst du hin? ("
		messages ["de"] [148] = " Grad; "
		messages ["de"] [149] = " Meter)"
		messages ["de"] [150] = "Rudolph ist begeistert. Seine kleinen Spielchen habe ich ja nun gerne mitgemacht. Aber es waere schoen fuer ihn, wenn wir seine Freunde nun endlich finden wuerden. Ich kann sie ihm nicht den ganzen Tag ersetzen, daher machen wir uns weiter auf die Suche nach seinen Kollegen. Achtung: Die Vase im Bild muss dort nicht zwingend stehen! Achtet auf den Torbogen!"
		messages ["de"] [151] = "Eine alte Dame in einem roten Mantel laeuft vor uns und hat diverse Taschen und Tueten in den Haenden. Sie traegt schwer. Ploetzlich reisst die eine Tuete und diverse Nuesse, Mandarinen, Aepfel und lauter gute Sachen verteilen sich auf dem Gehweg. Selbstverstaendlich helfe ich gerne und sammle alles mit auf. Aber wohin mit den Sachen?"
		messages ["de"] [152] = "Das Teil hilft mir nicht. Da muss ich noch weitersuchen."
		messages ["de"] [153] = "Rudolph braucht etwas Energie, die wird er hiervon sicherlich nicht bekommen!"
        messages ["de"] [154] = "Das Teil hilft mir jetzt wirklich nicht um den Weg zu finden"
        messages ["de"] [155] = [[Die alte Dame zwinkert Rudolph verschwoererisch zu. "Wenn ]]
        messages ["de"] [156] = [[ sich solche Muehe mit Dir und mir gegeben hat, dann wollen wir uns doch auch anstaendig bedanken, oder?" "Genau!" meint Rudolph. "Nochmal einen herzlichen Dank, dass Du mir geholfen hast. Und auch wenn ich Dir dieses kleine Geschenk nicht unter den Baum lege, so habe ich doch eine Kleinigkeit fuer Dich versteck! Du laeufst direkt darauf zu."

In der Luft hoere ich ein leises Klingeln und als wir darauf zugehen....]]
        messages ["de"] [157] = "Es gibt gerade hiermit nichts zu tun!"
        messages ["de"] [158] = [[... wird das Klingeln lauter. Von Ferne sehen wir einen Schlitten durch die Luft rasen. Acht Rentiere ziehen ihn muehelos. Leise wie die Schneeflocken landen sie vor uns. Der Frontplatz ist noch frei fuer Rudolph. Er laechelt mir noch einmal zu, zwinkert und ... wuenscht mir jetzt schon einmal "Frohe Weihnachten" und "einen schoenen Cachetag."]]
        messages ["de"] [159] = [[P.S. Nun duerft ihr den Final suchen. Leider ist im Stadtpark kaum eine sinnvolle und muggelfreie Finallocation zu finden und so sind wir mit unserem eigenen Final eher weniger gluecklich.
Sollte euch der Finalzugang auch nicht gefallen oder zu vermuggelt sein, koennt ihr gerne auch NUR mit Hilfe eures individuellen Unlockcodes (siehe Inventar) auf Wherigo.com loggen. Wer statt im Logbuch NUR dort loggt, ist aus unserer Sicht ebenfalls zum normalen Loggen auf Geocaching.com berechtigt.
Nochmals ein DANKE fuer das Spielen unseres Wherigos.]]
        messages ["de"] [160] = "Der Unlock-Code lautet (bei Fehler einfach das letzte Zeichen/Ziffer weglassen): "
        messages ["de"] [161] = "Das Teil hilft mir nicht. Da muss ich noch weitersuchen."
        messages ["de"] [162] = "Rudolph braucht etwas Energie, die wird er hiervon sicherlich  bekommen!"
        messages ["de"] [163] = "Das Teil hilft mir jetzt wirklich nicht um den Weg zu finden"
        messages ["de"] [164] = "Das hilft mir jetzt nun wirklich nicht!"
        messages ["de"] [165] = "Es gibt gerade hiermit nichts zu tun!"
        messages ["de"] [166] = "Das Teil hilft mir nicht. Da muss ich noch weitersuchen."
        messages ["de"] [167] = "Rudolph braucht etwas Energie, die wird er hiervon sicherlich bekommen!"
        messages ["de"] [168] = "Das Teil hilft mir jetzt wirklich nicht um den Weg zu finden"
        messages ["de"] [169] = "Das hilft mir jetzt nun wirklich nicht!"
        messages ["de"] [170] = "Es gibt gerade hiermit nichts zu tun!"
        messages ["de"] [171] = "Telefonzelle"
        messages ["de"] [172] = "Leider nicht mehr real vorhanden, deshalb haben wir hier eine virtuelle aufstellen."
    messages ["en"] = {}
        messages ["en"] [1] = ""
		messages ["en"] [2] = ""
		messages ["en"] [3] = ""
		messages ["en"] [4] = ""
		messages ["en"] [5] = ""
		messages ["en"] [6] = ""
		messages ["en"] [7] = ""
		messages ["en"] [8] = ""
		messages ["en"] [9] = ""
		messages ["en"] [10] = ""
		messages ["en"] [11] = ""
		messages ["en"] [12] = ""
		messages ["en"] [13] = ""
		messages ["en"] [14] = ""
		messages ["en"] [15] = ""
		messages ["en"] [16] = ""
		messages ["en"] [17] = ""
		messages ["en"] [18] = ""
		messages ["en"] [19] = ""
		messages ["en"] [20] = ""
		messages ["en"] [21] = ""
		messages ["en"] [22] = ""
		messages ["en"] [23] = ""
		messages ["en"] [24] = ""
		messages ["en"] [25] = ""
		messages ["en"] [26] = ""
		messages ["en"] [27] = ""
		messages ["en"] [28] = ""
		messages ["en"] [29] = ""
		messages ["en"] [30] = ""
		messages ["en"] [31] = ""
		messages ["en"] [32] = ""
		messages ["en"] [33] = ""
		messages ["en"] [34] = ""
		messages ["en"] [35] = ""
		messages ["en"] [36] = ""
		messages ["en"] [37] = ""
		messages ["en"] [38] = ""
		messages ["en"] [39] = ""
		messages ["en"] [40] = ""
		messages ["en"] [41] = ""
		messages ["en"] [42] = ""
		messages ["en"] [43] = ""
		messages ["en"] [44] = ""
		messages ["en"] [45] = ""
		messages ["en"] [46] = ""
		messages ["en"] [47] = ""
		messages ["en"] [48] = ""
		messages ["en"] [49] = ""
		messages ["en"] [50] = ""
		messages ["en"] [51] = ""
		messages ["en"] [52] = ""
		messages ["en"] [53] = ""
		messages ["en"] [54] = ""
		messages ["en"] [55] = ""
		messages ["en"] [56] = ""
		messages ["en"] [57] = ""
		messages ["en"] [58] = ""
		messages ["en"] [59] = ""
		messages ["en"] [60] = ""
		messages ["en"] [61] = ""
		messages ["en"] [62] = ""
		messages ["en"] [63] = ""
		messages ["en"] [64] = ""
		messages ["en"] [65] = ""
		messages ["en"] [66] = ""
		messages ["en"] [67] = ""
		messages ["en"] [68] = ""
		messages ["en"] [69] = ""
		messages ["en"] [70] = ""
		messages ["en"] [71] = ""
		messages ["en"] [72] = ""
		messages ["en"] [73] = ""
		messages ["en"] [74] = ""
		messages ["en"] [75] = ""
		messages ["en"] [76] = ""
		messages ["en"] [77] = ""
		messages ["en"] [78] = ""
		messages ["en"] [79] = ""
		messages ["en"] [80] = ""
		messages ["en"] [81] = ""
		messages ["en"] [82] = ""
		messages ["en"] [83] = ""
		messages ["en"] [84] = ""
		messages ["en"] [85] = ""
		messages ["en"] [86] = ""
		messages ["en"] [87] = ""
		messages ["en"] [88] = ""
		messages ["en"] [89] = ""
		messages ["en"] [90] = ""
		messages ["en"] [91] = ""
		messages ["en"] [92] = ""
		messages ["en"] [93] = ""
		messages ["en"] [94] = ""
		messages ["en"] [95] = ""
		messages ["en"] [96] = ""
		messages ["en"] [97] = ""
		messages ["en"] [98] = ""
		messages ["en"] [99] = ""
		messages ["en"] [100] = ""

function getMessage(language,number)
    return messages [language] [number]
end
